const path = require("path");

module.exports = {
    context: path.resolve("./"),
    entry: {},
    resolve: {
        modules: [
            "node_modules",
            "src",
            "."
        ],
        // extensions: ['.js', '.json', ".css", ".less", ".html"]
    },
    output: {
        path: path.resolve("./dist/assets"),
        publicPath: '/assets/'
    },
    plugins: [],
    module: {
        rules: [
            {
                test: /\.less$/,
                use: [
                    "style-loader/useable",
                    // "css-loader",
                    {
                        loader: "css-loader",
                        options: {
                            importLoaders: 2
                        }
                    },
                    "postcss-loader",
                    "less-loader",
                ]
            },
            {
                test: /\.css$/,
                use: [ "style-loader/useable", "css-loader", "postcss-loader" ]
            },
            {
                test: /\.(png|jpg|svg)$/,
                loader: "url-loader?limit=100000"
            },
            {
                test: /\.html$/,
                use: [{
                    loader: "html-loader",
                    options: {
                        minimize: true,
                        collapseWhitespace: true,
                        collapseInlineTagWhitespace: true,
                        conservativeCollapse: false
                    }
                }]
            },
            // { test: /\.json$/, loader: "json-loader" }
        ]
    }
};
