/*
 * the last three modify :
 *   soulran        2017-06-09
 *   soulran        2016-05-17
 *
 */

const log = require('./report.js').log;
const report = require('./report.js').report;
const trigger = require('./report.js').trigger;
const path = require('path');

// config

const tasker = require('./tasker.js');
const taskJs = require('./js.js');
const taskHtml = require('./html.js');

// deal with argv
let env = 'dev';
const params = {
    production: function(){
        env = "pro";
    },
    watch: function(){
        env = "watch";
    }
};
const sparams = {
    p: "production",
    w: "watch"
};
const argv = process.argv.slice(2);
// parse argv
argv.forEach(function(arg){
    let param = false;
    if(arg.slice(0, 2) === '--')param = arg.slice(2);
    else if(arg[0] === '-'){
        let sparam = arg.slice(1);
        param = sparams[sparam];
    }
    if(param){
        params[param]();
    }
});

// run build
switch(env){
    case 'dev':
    case 'pro':
        buildProject(env);
        break;
    case 'watch':
        log("  Watch task start, press ctrl + c to stop.\n", 'info');
        let callback = [() => trigger('watch-ok'), () => trigger('watch-error')];
        taskJs.watch(callback);
        taskJs.getEntries().then(entries => {
            for(let key in entries){
                entries[key] = key + '.js';
            }
            taskHtml.watch(entries, callback);
        });
        break;
}

function buildProject(env){
    log('');
    report("Starting Build Project", 'help');
    return taskJs.build(env)
        .then(
            revJson => taskHtml.build(env, revJson),
            reason => Promise.reject(reason)
        )
        .then(
            () => report("Complete Build Project", 'help'),
            reason => trigger('build-error')
        );
}
