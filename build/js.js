const log = require('./report.js').log;
const tasker = require('./tasker.js');
const gulp = require('gulp');
const path = require('path');
const fs = require('fs');
// const exec = require('child_process').exec;
const glob = require('glob');
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const webpackConfig = require('./webpack.common.js');

// config
const TARGETS = '[jv]s';

// all task
const task = {};
module.exports = task;

// build task
task.build = function(env){
    return tasker('Build Js', (resolve, reject) => {
        switch(env){
            case 'dev':
            case 'pro':
                task.getEntries()
                .then(entries => task.webpack(env, entries), reject)
                .then(res => {
                    const { err, stats, entries } = res;
                    const assets = stats.toJson().assets;
                    const revJson = {};
                    assets.forEach(item => {
                        let name = item.chunkNames[0];
                        if(!name){
                            log('[Warn] You should declare chunkNames to ensure putting chunk file into suitable position.', 'warn');
                            return;
                        }
                        if((name in entries) && item.name.split('.').pop() === 'js'){
                            revJson[name] = item.name;
                        }
                    });
                    resolve(revJson);
                }, reject);
                break;
            default:
                break;
        };
    });
};

// get entries
task.getEntries = function(){
    return tasker('Get Entries', function(resolve, reject){
        const tailLength = -'/entry.js'.length;
        glob(path.resolve("./src", '**/entry.js'), function(err, files){
            const entries = {};
            files.forEach(function(file){
                entries[file.slice(path.resolve("./src").length + 1, tailLength)] = file;
            });
            resolve(entries);
        });
    });
};

// webpack
task.webpack = function(env, entries){
    return tasker('Webpack', function(resolve, reject){
        const config = require('./webpack.' + env + '.js');
        const buildConfig = webpackMerge(webpackConfig, config);
        buildConfig.entry = entries;

        // TODO webpack watch
        buildConfig.watch = true,
        buildConfig.watchOptions = {
            aggregateTimeout: 300,
            ignored: /node_modules/,
            // poll: true,
        };

        webpack(buildConfig, function(err, stats){
            if(err || stats.hasErrors() || stats.hasWarnings()){
                log('Start of Errors: ', 'warn');
                if(err){
                    console.error(err.stack || err);
                    if(err.details)console.error(err.details);
                } else if(stats.hasErrors()){
                    stats.toJson().errors.forEach(function(err){
                        console.error(err);
                    });
                } else if(stats.hasWarnings()){
                    stats.toJson().warnings.forEach(function(warning){
                        console.warn(warning);
                    });
                }
                log('End of Errors.', 'warn');
                reject({ err, stats, entries: buildConfig.entry });
            } else {
                stats.toJson().assets.forEach(asset => {
                    console.log('Finished', asset.name, '\x1B[33m' + ~~(asset.size / 1024) + 'kb\x1B[39m');
                });
                resolve({ err, stats, entries: buildConfig.entry });
            }
        });
    });
};

// watch
task.watch = function(callback){
    gulp.watch([path.resolve('src/**/*')], function(ev){
        log("File [..." + ev.path.slice(-36) + "] changes. Building......");
        let dirs = path.dirname(ev.path).split(path.sep), rootNum = path.dirname("./").split(path.sep).length;
        while(!fs.existsSync(dirs.concat(["entry.js"]).join(path.sep)) && rootNum < dirs.length){
            dirs.pop();
        }
        dirs.push("entry.js");
        dirs = dirs.join(path.sep);
        if(!fs.existsSync(dirs)){
            // build all
            task.build('dev').then(...callback);
            return;
        }
        task.webpack('dev', {
            [path.dirname(path.relative("./src", dirs))]: dirs
        }).then(...callback);
    });
}
