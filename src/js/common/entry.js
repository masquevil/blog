(function(){
    require("src/less/index.less").use();

    var $ = require('ether-vway');
    var marked = require('marked');
    var hljs = require(/* webpackChunkName: "js/lib/highlight/index" */ 'highlight.js/lib/highlight');
    var hljsLang = {
        javascript: require(/* webpackChunkName: "js/lib/highlight/javascript" */ 'highlight.js/lib/languages/javascript'),
    };
    hljs.registerLanguage('javascript', hljsLang.javascript);

    Vue.use($);
    Vue.prototype.$date = $.date;

    // markdown
    var renderer = new marked.Renderer();
    renderer.code = function(code, lang, escaped) {
        var highlight = this.options.highlight(code, lang);
        return '<pre><code class="hljs" data-lang="' + highlight.language + '">' + highlight.value + "\n</code></pre>\n";
    };
    marked.setOptions({
        renderer: renderer,
        highlight: (code, lang) => hljs.highlightAuto(code),
    });

    $.config({
        shortcut: '$',
        globalData: {
            name: "G",
        },
        // 注册 api 统计参数，注册响应事件
        api: {
            pretreater: url => url,
            success: function(response){
                switch (response.body.code) {
                    case 0:
                        response.body.data.__msg = response.body.message;
                        return Promise.resolve(response.body.data);
                    // 通用：通用错误
                    case 1:
                        break;
                    // 通用：登录过期
                    case 10:
                        break;
                    default: break;
                }
                return Promise.reject({ type: 'code', body: response.body });
            },
            error: function(response){
                $.popper('网络异常，请重试 :::>_<:::', 'warning');
                return Promise.reject({ type: 'network', response: response });
            }
        },
        // 注册布局
        layout: {
            layouts: {
                'base': require('./_layout/base/i.js')
            },
            // title: '',
            // suffix: '',
            defaultLayout: 'base',
            pageMixins: []
        },
        // 注册路由
        router: {
            routes: require('./_router/route.config.js').default,
            pretreater: function(file){
                return '/assets/' + getDist(file);
            },
            autoCut: false,
            beforeJump: function(path){
                document.body.scrollTop = document.documentElement.scrollTop = 0;
            },
            configure: {
                before: function(){
                    // if login, do nothing before route. else prevent route match and go to the login page.
                    // if($.ls.getGlobal('TOKEN') || location.pathname === '/source/login')return;
                    // this.replaceRoute('/source/login?after_login=' + encodeURIComponent(location.pathname + location.search));
                    // return false;
                },
                on: function(){
                    // _hmt.push(['_trackPageview', location.pathname + location.search]);
                }
            }
        }
    });

    $.init();

    // var UI = require("src/unique-interface/src/index.js");
    // Vue.use(UI);
})();
